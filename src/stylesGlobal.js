import { Dimensions } from "react-native";
import { StyleSheet } from "react-native";

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);


export default StyleSheet.create({


  
    // Page Principal 

    page:{
        backgroundColor: "#FBFBFB",
        flex: 1
    },

    // Container Principal 


    container: {
        paddingHorizontal: "4%",
        paddingTop: '4%',
        paddingBottom: '4%',
    },

    // Titulo Principal

    titleStyle:{
        fontSize: 18,
        color: "#182133",
        fontFamily: 'Open Sans',

    },

    text:{
        fontSize: 18,
        color: "#182133",
        fontFamily: 'Open Sans SemiBold',
        
    },


    subText :{
        fontSize:16,
        color:'#414141',
        fontFamily: 'Open Sans SemiBold',
    },

    inputStyle:{
       height: 50, 
       borderColor: 'transparent', 
       borderWidth: 1,
       backgroundColor:"#EBEBED",
       paddingHorizontal:20,
       borderRadius:5,
       marginVertical:8,
       fontFamily: 'Open Sans',

    },
    textArea:{
        height: 200, 
        borderColor: 'transparent', 
        borderWidth: 1,
        backgroundColor:"#EBEBED",
        paddingHorizontal:20,
        borderRadius:5,
        marginVertical:8,
        textAlignVertical: 'top',
        fontFamily: 'Open Sans',

     }

})