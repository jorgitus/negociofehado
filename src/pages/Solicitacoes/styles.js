








import { StyleSheet } from "react-native";



export default StyleSheet.create({

    container: {
        paddingHorizontal: "6%",
        paddingBottom: "10%",
        backgroundColor: "#ecf1f8",
        flex: 1,


    },

    page: {
        backgroundColor: "#ecf1f8",
     


    },

    headerContainer: {
        paddingHorizontal: "6%",
        height: 30,
        flex: 1
    },


    categorias: {
        paddingHorizontal: "6%",

        marginTop: -40,
    },
    headerText: {
        fontSize: 20,
        fontWeight: "500",
        color: "#fff",
    },

    logo: {
        fontSize: 20,
        color: "#fff"
    },
    voltar: {
        marginRight: 25
    },
    title: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#fff"
    },
    headerTextBolder: {
        fontWeight: "bold",
        color: "#f1f1f1"

    },
    description: {
        fontSize: 18,
        lineHeight: 24,
        color: "#fff"
    },
    cardLogo: {
        alignItems: "center",
        justifyContent: "center"
    },
    card: {
        paddingVertical: 22,
        paddingHorizontal: 12,

        justifyContent: "flex-start",
        backgroundColor: "#fff",
        marginTop: 12,
        borderRadius: 8,
        flexDirection: "row"

    },
    infos: {
        marginHorizontal: "15%",
        paddingRight: 70,
        alignItems: "flex-start",
        justifyContent: "flex-start",

    },
    range: {
        flexDirection: "row",
        alignContent: "center"

    },
    subinfo: {
        color: '#2c2c2c',
        fontSize: 11
    },
    nav: {
        marginBottom: '5%',
        marginTop: '5%',
    },
    flatlist: {
        height: 300,
        marginBottom: 12
    },
    mapa: {
        paddingVertical: 22,
        borderRadius: 8,
        height: 300,
        marginTop: -25

    },



    description: {
        color: '#fff',
        fontSize: 16,
        marginTop: 4,
        fontFamily: 'Roboto_400Regular',
    },

    search: {


        backgroundColor: "#fff",
        borderRadius: 8
    },

    searchTitle: {
        fontSize: 18,
        fontWeight: "600",
        color: "#222",
    },




    itemsContainer: {
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 22,
    },

    item: {
        backgroundColor: '#fcfcfc',
        borderWidth: 2,
        borderColor: '#ffffff',
        height: 90,
        width: 90,
        borderRadius: 6,
        paddingHorizontal: 10,
        paddingTop: 20,
        paddingBottom: 16,
        marginRight: 8,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
    },

    selectedItem: {
        borderColor: '#34CB79',
        borderWidth: 2,
    },

    itemTitle: {
        fontFamily: 'Roboto_400Regular',
        textAlign: 'center',
        fontSize: 10
    },

    infotitle: {
        fontSize: 14
    }
})