import React, { Component } from 'react';
import {
    View,
    ScrollView,
    TouchableOpacity
} from 'react-native';

import { Icon, Text } from 'react-native-elements';
import styles from "./styles";
import { useNavigation } from "@react-navigation/native";
import { CardSolicitacao } from "../../components/Card";
import TopBar from "../../components/TopBar"
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

export default function Home() {

    const TopTab = createMaterialTopTabNavigator();
    const navigation = useNavigation();

    function handleNavigation() {
        navigation.navigate('MinhaLoja');
    }

    function handleLocation() {
        navigation.navigate('Localizacao');
    }

    function handleDrawer() {
        navigation.openDrawer();
    }

    function Todas() {
        return (
            <>
                <View style={styles.page}>


                    <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>


                        <View>

                            <View style={styles.container}>


                                <CardSolicitacao actions={true} title="ESCAVADEIRA HIDRAULICA" description="FORNECEDORA MAQUINAS" periodo="10/11/2000 - 12/12/2000" image={require('../../assets/escavadeiraCase.jpg')} />

                                <CardSolicitacao actions={true} title="ESCAVADEIRA HIDRAULICA" description="FORNECEDORA MAQUINAS" periodo="10/11/2000 - 12/12/2000" image={require('../../assets/escavadeiraCase.jpg')} />

                                <CardSolicitacao actions={true} title="ESCAVADEIRA HIDRAULICA" description="FORNECEDORA MAQUINAS" periodo="10/11/2000 - 12/12/2000" image={require('../../assets/escavadeiraCase.jpg')} />

                                <CardSolicitacao actions={true} title="ESCAVADEIRA HIDRAULICA" description="FORNECEDORA MAQUINAS" periodo="10/11/2000 - 12/12/2000" image={require('../../assets/escavadeiraCase.jpg')} />

                            </View>
                        </View>
                    </ScrollView>
                </View>


            </>
        )
    }

    function Pendentes() {
        return (
            <>
                <View style={styles.page}>


                    <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>


                        <View>

                            <View style={styles.container}>


                                <CardSolicitacao  title="ESCAVADEIRA HIDRAULICA" description="FORNECEDORA MAQUINAS" periodo="10/11/2000 - 12/12/2000" image={require('../../assets/escavadeiraCase.jpg')} />

                                <CardSolicitacao title="ESCAVADEIRA HIDRAULICA" description="FORNECEDORA MAQUINAS" periodo="10/11/2000 - 12/12/2000" image={require('../../assets/escavadeiraCase.jpg')} />

                                <CardSolicitacao  title="ESCAVADEIRA HIDRAULICA" description="FORNECEDORA MAQUINAS" periodo="10/11/2000 - 12/12/2000" image={require('../../assets/escavadeiraCase.jpg')} />

                                <CardSolicitacao title="ESCAVADEIRA HIDRAULICA" description="FORNECEDORA MAQUINAS" periodo="10/11/2000 - 12/12/2000" image={require('../../assets/escavadeiraCase.jpg')} />

                            </View>
                        </View>
                    </ScrollView>
                </View>


            </>
        )
    }

    return (
        <>
            <View style={{   flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#ff8c1f",
        paddingHorizontal: "6%",
        paddingTop: "15%",
        paddingBottom: "4%",}}>
                <TouchableOpacity style={styles.voltar} onPress={handleNavigation}>
                    <Icon name='arrow-left' type='feather' size={28} color='#fff' />
                </TouchableOpacity>

                <Text style={styles.headerText}>Minha Loja</Text>
            </View>

            <TopTab.Navigator tabBarOptions={{
                labelStyle: { fontSize: 14, fontWeight: "600" },
                tabStyle: { width: 120 },
                style: { backgroundColor: '#ff8c1f' },
                inactiveTintColor: "#f1f1f1",
                activeTintColor: "#fff",
                indicatorStyle: { backgroundColor: "#fff" },
                scrollEnabled: true
            }}>
                <TopTab.Screen name="Pendentes" component={Todas} />
                <TopTab.Screen name="Ativas" component={Pendentes} />
                <TopTab.Screen name="Canceladas" component={Pendentes} />



            </TopTab.Navigator>

        </>
    )
}

