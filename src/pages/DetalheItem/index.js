import React, { Component } from 'react';
import {
    View,
    Text,
    ImageBackground,
    Image,
    SafeAreaView,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { Icon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';

import { RectButton } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import styles from "./styles";
import TopBar from "../../components/TopBar"
import StylesGlobal from '../../stylesGlobal';


const Fornecedor = () => {

    const navigation = useNavigation();

    function handleNavigation() {
        navigation.navigate('Login');
    }

    function handleNavigationback() {
        navigation.navigate('Home');
    }

    return (

        <SafeAreaView>
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                <Animatable.View duration={1100} animation="bounceInDown" >
                <ImageBackground style={{ width: "100%", height: 350, borderRadius: 8, }} source={require('../../assets/escavadeiraCase.jpg')}>
                <View style={styles.innerFrame}>
                <TopBar>
                        <RectButton onPress={handleNavigationback} style={styles.voltar}>
                            <Icon name='arrow-left' type='feather' size={26} color='#fff' />
                        </RectButton>
                        {/* <Text style={styles.headerText}>DETALHES DO ITEM</Text> */}
                    </TopBar>
                </View>
               
                </ImageBackground>
                </Animatable.View>
                <Animatable.View duration={300} animation="fadeInUpBig" style={styles.details}>
                    <View>
                        <Text style={styles.cardTitle}>
                            ESCAVADEIRAS HIDRÁULICAS
                                        </Text>
                        <Text style={styles.cardModelo}>CX130B</Text>

                        <View  style={{paddingVertical:12}}>
    
                            <Text style={StylesGlobal.subText}>Escavadeira Hidráulica ano 2016, modelo Case CX130B 46000 horas rodadas. Maquina com direção hidráulica, damos suporte mecânico. O transporte fica a encargo do cliente.</Text>
                            
                        </View>
                        <View style={styles.selects}>
                            <View style={styles.selectItem} >
                                <Image style={{ width: 14, height: 18, marginRight: 5 }} source={require('../../assets/checkbox.png')} />
                                <Text style={StylesGlobal.subText}>Sem taxas de Alteração</Text>
                            </View>
                            <View style={styles.selectItem} >
                                <Image style={{ width: 14, height: 18, marginRight: 5 }} source={require('../../assets/checkbox.png')} />
                                <Text style={StylesGlobal.subText}>Taxas Administrativas</Text>
                            </View>
                            <View style={styles.selectItem} >
                                <Image style={{ width: 14, height: 18, marginRight: 5 }} source={require('../../assets/checkbox.png')} />
                                <Text style={StylesGlobal.subText}>Garantia</Text>
                            </View>
                        </View>
                    </View>
                    <View>
                    </View>
                    <View style={styles.footer}>
                    <RectButton style={styles.entrar} onPress={handleNavigation}>
                        <Text style={styles.entrarText}>Cotação</Text>
                    </RectButton>
                </View>
                </Animatable.View>
            </ScrollView>
        </SafeAreaView>
    )

}

export default Fornecedor;


