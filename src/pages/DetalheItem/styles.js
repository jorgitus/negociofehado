import {StyleSheet} from "react-native";

import { Dimensions } from 'react-native';


const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

export default StyleSheet.create({

    container:{


        paddingBottom:30,
        backgroundColor:"#ecf1f8",
        flex:1
    },
    page: {
        backgroundColor: "#ecf1f8",
        flex:1,
        paddingHorizontal:"6%",
        marginTop:-12
        // paddingTop:16



    
    },
    
  header: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingHorizontal: "6%",
    paddingTop: "10%",
    paddingBottom: "4%",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  
   



  },
    headerText:{
        fontSize:20,
        fontWeight:"500",
        color:"#fff",
    },
    logo:{
        fontSize:20,
        color: "#fff"
    },
    innerFrame: {
        flex: 1, 
      
        backgroundColor: 'rgba(0, 0, 0, .31)', 
    },
    title:{
        fontSize:18,
        fontWeight:"500",
        color:"#222",
    },
    headerTextBolder:{
       fontWeight:"bold",
       color:"#f1f1f1"

    },
    description:{
        fontSize:16,
        lineHeight:24,
        color:"#222"
    },
    card:{
        paddingVertical:22,
        backgroundColor:"#fff",
        marginTop:15,
        borderRadius:8,
        padding:12
        
    },
    button:{
        backgroundColor:"#fff",
        padding:12,
        borderRadius:6,
        alignItems:"center",
        marginTop:10,
        flexDirection:'row',
       
    },
    buttonText:{
        color:"#222",
        fontWeight:"300",
        fontSize:16,
       
    },
    voltar:{
        marginRight:25
    },
    desc:{
        color:"#ddd",
        fontWeight:"300",
        fontSize:12,
    },
    entrar:{
        backgroundColor:"#fbbe4c",
        padding:12,
        borderRadius:6,
        alignItems:"center",
        marginVertical:8,
        width:"100%"
    },

    entrarText:{
        color:"#fff",
        fontWeight:"500",
        fontSize:16,
    },
    footer:{
         marginTop:16,
         marginBottom:16
    },
  
    cardModelo:{
        color:"#FF6B00",
        fontSize:22

    },

    cardTitle:{
        fontWeight:"bold",
        fontSize:22
    },
    typesBtn:{
        marginRight:22
    },
    types:{
        flexDirection:'row',
        marginTop:12
    },
    details:{
        marginTop:"-6%",
        backgroundColor:"#ffffff",
        padding:10,
        paddingTop:"10%",
        height:windowHeight/1.6,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        justifyContent:'space-between'
    },
    selects:{
        marginVertical:10
    },
    selectItem:{
        flexDirection:'row',
        marginTop:5
    },
   
    
    
})