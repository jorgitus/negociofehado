import React, {Component} from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Image,
  FlatList,
} from 'react-native';

import {Icon, Text} from 'react-native-elements';
import {CardProduct} from '../../components/Card';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import TopBar from '../../components/TopBar';
import StylesGlobal from '../../stylesGlobal';

export default function Categorias({route}) {
  const navigation = useNavigation();

  function handleNavigation() {
    navigation.navigate('DetalheItem');
  }

  function handleOrcamento() {
    navigation.navigate('Orcamento');
  }

  function handleHome() {
    navigation.navigate('Home');
  }
  const {categoria} = route.params;

  const ListHeader = (
    <ScrollView
      style={styles.categorias}
      horizontal
      showsHorizontalScrollIndicator={false}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#F8A125', '#fbbe4c']}
        style={styles.item}>
        <Image
          style={styles.imageItem}
          source={require('../../assets/icons/retroescavadeira1.png')}
        />
        <Text style={styles.itemTitle}>Escavadeira</Text>
      </LinearGradient>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#F8A125', '#fbbe4c']}
        style={styles.item}
        onPress={handleNavigation}>
        <Image
          style={styles.imageItem}
          source={require('../../assets/icons/empilhadeira1.png')}
        />
        <Text style={styles.itemTitle}>Empilhadeira</Text>
      </LinearGradient>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#F8A125', '#fbbe4c']}
        style={styles.item}
        onPress={() => {}}>
        <Image
          style={styles.imageItem}
          source={require('../../assets/icons/retro.png')}
        />
        <Text style={styles.itemTitle}>Retroescavadeira</Text>
      </LinearGradient>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#F8A125', '#fbbe4c']}
        style={styles.item}
        onPress={() => {}}>
        <Image
          style={styles.imageItem}
          source={require('../../assets/icons/miniescavadeira1.png')}
        />

        <Text style={styles.itemTitle}>Miniescavadeira</Text>
      </LinearGradient>
    </ScrollView>
  );

  return (
    <>
      <SafeAreaView style={StylesGlobal.page}>
        <TopBar>
          <TouchableOpacity style={styles.voltar} onPress={handleHome}>
            <Icon name="arrow-left" type="feather" size={26} color="#182133" />
          </TouchableOpacity>

          <Text style={StylesGlobal.text}>{categoria}</Text>
        </TopBar>

    
          <View style={StylesGlobal.container}>
            <FlatList
              showsVerticalScrollIndicator ={false}
              showsHorizontalScrollIndicator={false}
              data={[
                {key: 6, nome: 'ESCAVADEIRA HIDRÁULICA CX130B'},
                {key: 5, nome: 'ESCAVADEIRA HIDRÁULICA CX130B'},
                {key: 7, nome: 'ESCAVADEIRA HIDRÁULICA CX130B'},
                {key: 8, nome: 'ESCAVADEIRA HIDRÁULICA CX130B'},
              ]}
              ListHeaderComponent={ListHeader}
              renderItem={({item}) => (
                <CardProduct
                  title={item.nome}
                  logoTitle={require('../../assets/case.png')}
                  fornecedor="Fornecedora Maquinas"
                  itemImage={require('../../assets/escavadeiraCase.jpg')}
                  rating={require('../../assets/stars.png')}
                  detail={handleNavigation}
                  orcamento={handleOrcamento}
                />
              )}
            />
          </View>
     
      </SafeAreaView>
    </>
  );
}
