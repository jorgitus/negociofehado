import { StyleSheet } from "react-native";



export default StyleSheet.create({

  headerText: {
    fontSize: 20,
    fontFamily: "Roboto",
    color: "#182133",
  },

  itemsContainer: {
    flexDirection: 'row',
    marginTop: 16,
    marginBottom: 22,

  },

  item: {
    height: 95,
    width: 105,
    borderRadius: 6,
    paddingHorizontal: 2,
    paddingTop: 15,
    paddingBottom: 16,
    margin: 6,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'center',
    backgroundColor: "#FFF",
    shadowColor: "#3F52F2",
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,
    elevation: 3
  },

  itemTitle: {
  

    textAlign: 'center',
    fontSize: 12.5,
    paddingTop:3,
    color: '#fff',
    fontWeight: '600',
    fontFamily: 'Roboto',
  },


  categorias: {
    paddingVertical: "2%",
    paddingLeft: "2%",


  },

  text: {
    fontSize: 16,
    color: "#182133",
    fontFamily: "Roboto",
    fontWeight: "600",
    marginLeft: "4%",

  },

  imageItem: {
    width: 48,
    height: 48,
  },

  voltar: {
    marginRight: 25
  },






})