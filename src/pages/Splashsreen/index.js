import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  StatusBar,
  Image
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useTheme } from '@react-navigation/native';

const SplashScreen = ({ navigation }) => {
  const { colors } = useTheme();

  return (
    <View style={styles.container}>
      {/* <StatusBar backgroundColor='#FF6347' barStyle="light-content" /> */}
      <View style={styles.header}>
        <Animatable.Image
          animation="bounceIn"
          duraton="500"
          source={require('../../assets/logo2.jpeg')}
          style={styles.logo}
          resizeMode="stretch"
        />
      </View>
      <Animatable.View
        style={[styles.footer, {
          backgroundColor: colors.background
        }]}
        animation="fadeInUpBig"
      >
        <Text style={[styles.title, {
          color: colors.text
        }]}>Alugue e compre maquinas de qualquer lugar do brasil</Text>
        <Text style={styles.text}>Entrar com sua conta</Text>
        <View style={styles.button}>
          <TouchableOpacity onPress={() => navigation.navigate('DrawerScream')}>
            <LinearGradient
              colors={['#fbbe4c', '#fbbe4c']}
              style={styles.signIn}
            >
              <Text style={styles.textSign}>Iniciar</Text>
              <MaterialIcons
                name="navigate-next"
                color="#fff"
                size={20}
              />
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  );
};

export default SplashScreen;

const { height } = Dimensions.get("screen");
const height_logo = height * 0.2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2F3649'
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  footer: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingVertical: 50,
    paddingHorizontal: 30
  },
  logo: {
    width: height_logo*1.8,
    height: height_logo
  },
  title: {
    color: '#05375a',
    fontSize: 24,
    fontFamily: 'Open Sans SemiBold',
  },
  text: {
    color: 'grey',
    marginTop: 5,
    fontFamily: 'Open Sans',

  },
  button: {
    alignItems: 'flex-end',
    marginTop: 30
  },
  signIn: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row'
  },
  textSign: {
    color: 'white',
    fontFamily: 'Open Sans',

  }
});

