


import {StyleSheet} from "react-native";
import { Dimensions } from 'react-native';


const windowHeight = Dimensions.get('window').height;

export default StyleSheet.create({

    backgroundVideo: {
        height: windowHeight+35,
        position: "absolute",
        top: 0,
        left: 0,
        alignItems: "stretch",
        bottom: 0,
        right: 0
      },
      container:{
          flex:1,
          justifyContent:'center',
          alignItems:'center',
          textAlign:'center',
          marginTop:windowHeight - 470,
        
        
      },
      ImageBackground:{
        marginBottom:150
      },
     
      title:{
        color: '#f4f4f4',
        marginHorizontal:12,
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        letterSpacing: 3,
       
      },

      span1:{
        color:"#333",
        fontWeight: 'bold',
        paddingRight:30,
        textShadowColor:'#fff',
        textShadowOffset:{width: 1, height: 1},
        textShadowRadius:3,
       
      },
      span2:{
        color:"#F69A38",
        fontWeight: 'bold',
      },
      
      texts:{
        textAlign: 'center',
       
          justifyContent:"center",
          alignItems:'center'
      }

})

console.log(windowHeight)