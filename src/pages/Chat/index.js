import React from 'react';
import {TouchableOpacity} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';
import TopBar from '../../components/TopBar';
import styles from './styles';
import {Icon, Text,View} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';

export default function Home() {
  const navigation = useNavigation();

  function handleNavigation() {
    navigation.navigate('Home');
  }

  return <Example navigation={() => handleNavigation()} />;
}
class Example extends React.Component {
  constructor(props) {
    super(props);
    this.state = {messages: []};
    this.onSend = this.onSend.bind(this);
  }
  UNSAFE_componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Bom dia, queria alugar uma escavadeira, no periodo de 30/09 ate 05/11.',
          createdAt: new Date(Date.UTC(2020, 7, 30, 17, 20, 0)),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://facebook.github.io/react/img/logo_og.png',
          },
          sent: true,
          // Mark the message as received, using two tick
          received: true,
          // Mark the message as pending with a clock loader
          pending: true,
        },
      ],
    });
  }
  onSend(messages = []) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });
  }
  render() {
    return (
      <>
        <TopBar>
          <TouchableOpacity style={styles.voltar} onPress={this.props.navigation}>
            <Icon name="arrow-left" type="feather" size={28} color="#000" />
          </TouchableOpacity>

          <Text style={styles.headerText}>Conversas</Text>
        </TopBar>
  
        <GiftedChat

          messages={this.state.messages}
          onSend={this.onSend}
          user={{
            _id: 1,
          }}
        />
       
        
      </>
    );
  }
}
