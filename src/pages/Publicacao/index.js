import React, { Component } from 'react';
import {
    View,
    Text,
    ImageBackground,
    Image,
    SafeAreaView,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { Icon, Input, CheckBox } from 'react-native-elements';

import { RectButton } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import styles from "./styles";

const Detalhe = () => {

    const navigation = useNavigation();

    function handleNavigation() {
        navigation.navigate('Login');
    }

    function handleNavigationback() {
        navigation.goBack();
    }

    return (

        <>

            <View style={styles.container}>
                <ScrollView>

                    <View style={styles.header}>
                        <RectButton style={styles.voltar} onPress={handleNavigationback}>
                            <Icon name='arrow-left' type='feather' size={26} color='#222' />
                        </RectButton>
                        <Text style={styles.title}>Publicação</Text>
                    </View>

                    <View style={styles.body}>
                        <View style={styles.details}>
                            <View>
                                <View style={styles.orcamentoHeader}>
                                    <Image style={{ borderRadius: 8, marginRight: 12 }} source={require('../../assets/escavadeiraCase.jpg')} />
                                </View>


                                <View style={styles.descricao}>
                                    <Text>Marca :</Text>
                                    <Text>Tipo :</Text>
                                    <Text>Modelo :</Text>
                                    <Text>Descrição:</Text>

                                </View>

                                <View>
                                    <View style={styles.item}>
                                        <Icon name='edit' type='feather' size={26} color='#222' />
                                        <Text style={styles.itemTitle2}>
                                            TIPO DE CONTRATO
                                    </Text>

                                    </View>
                                    <View style={styles.opcoes}>
                                        <CheckBox
                                            center
                                            title='Máquina'
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                        />

                                        <CheckBox
                                            center
                                            title='Máquina e Operador'
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                        />
                                    </View>
                                </View>


                                <View style={styles.item}>
                                    <Icon name='dollar-sign' type='feather' size={26} color='#222' />
                                    <Text style={styles.itemTitle2}>
                                        VALOR POR HORA
                                </Text>
                                </View>


                                <View style={styles.item}>
                                    <Icon name='award' type='feather' size={26} color='#222' />
                                    <Text style={styles.itemTitle2}>
                                        VANTAGENS
                                </Text>
                                </View>

                                <View style={styles.selects}>
                                    <View style={styles.selectItem} >
                                        <CheckBox
                                            title='Frete'
                                        />
                                        <CheckBox
                                            title='Operador'
                                        />
                                        <CheckBox
                                            title='Seguro'
                                        />
                                    </View>
                                </View>
                            </View>
                            <View>
                            </View>
                        </View>
                    </View>

                    <View style={styles.footer}>
                        <RectButton style={styles.entrar} onPress={handleNavigation}>
                            <Text style={styles.entrarText}>Prosseguir</Text>
                        </RectButton>
                    </View>


                </ScrollView>
            </View>

        </>
    )

}

export default Detalhe;


