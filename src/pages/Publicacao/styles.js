import {StyleSheet} from "react-native";



export default StyleSheet.create({

    container:{
        paddingHorizontal:24,
        paddingTop:25,
        paddingBottom:27,
        backgroundColor:"#fff",
        flex:1
    },
    header:{
        marginTop:60,
        flexDirection:"row",
        alignItems:"center",
        marginBottom:25,
    },
    headerText:{
        fontSize:15,
        color: "#fff"
    },
    orcamentoHeader:{
        flexDirection:"row",
        alignItems:'center',
        justifyContent:'space-around',
    },

    item:{
        flexDirection:"row",
        alignItems:'center',
        marginVertical:10
        
    },
    descricao:{
        backgroundColor:"#fff",
        padding:22

    },
    detalhes:{
        flexDirection:"row"
    },
    logo:{
        fontSize:20,
        color: "#fff"
    },
    title:{
        fontSize:18,
        fontWeight:"500",
        color:"#222",
    },
    headerTextBolder:{
       fontWeight:"bold",
       color:"#f1f1f1"

    },
    opcoes:{
        flexDirection:"row"
        
    },
    description:{
        fontSize:16,
        lineHeight:24,
        color:"#222"
    },
    card:{
        paddingVertical:22,
        backgroundColor:"#fff",
        marginTop:15,
        borderRadius:8,
        padding:12
        
    },
    button:{
        backgroundColor:"#fff",
        padding:12,
        borderRadius:6,
        alignItems:"center",
        marginTop:10,
        flexDirection:'row'
    },
    buttonText:{
        color:"#222",
        fontWeight:"300",
        fontSize:16,
       
    },
    voltar:{
        marginRight:25
    },
    desc:{
        color:"#ddd",
        fontWeight:"300",
        fontSize:12,
    },
    entrar:{
        backgroundColor:'#FCB031',
        padding:12,
        borderRadius:4,
        justifyContent:'center',
        alignItems:'center'
    },

    entrarText:{
        color:"#fff",
        fontWeight:"500",
        fontSize:16,
    },
    footer:{
         marginTop:16
    },
 
  
    cardModelo:{
        color:"#FF6B00"
    },

    cardTitle:{
        fontWeight:"bold",
    },
    itemTitle2:{
        fontWeight:"bold",
        paddingHorizontal:22
    },

    itemTitle:{
        fontWeight:"bold",
      
    },
    typesBtn:{
        marginRight:22,
        alignItems:'center'
    },
    types:{
        flexDirection:'row',
        marginTop:12
    },
    details:{
        marginTop:15,
      
    },
    selects:{
        marginVertical:10
    },
    selectItem:{
        flexDirection:'row',
        marginTop:5,
        alignItems:'center'
    },
   
    
    
})