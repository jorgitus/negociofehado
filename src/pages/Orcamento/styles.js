import {StyleSheet} from "react-native";
import { Dimensions } from 'react-native';


const win = Dimensions.get('window');


export default StyleSheet.create({

   
   
  
    item:{
        flexDirection:"column",
    },
   
    content:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:'space-around',
        paddingVertical:6
    },

    itemHeader:{
        flexDirection:"row",
        alignItems:'center',
    },
    descricao:{
        backgroundColor:"#fff",
        padding:22

    },
    detalhes:{
        flexDirection:"row"
    },
    logo:{
        fontSize:20,
        color: "#fff"
    },
    title:{
        fontSize:20,
        fontWeight:"500",
        color:"#fff",
    },
    headerTextBolder:{
       color:"#f1f1f1",
       marginLeft:5

    },
    description:{
        fontSize:16,
        lineHeight:24,
        color:"#222"
    },
   
    button:{
        backgroundColor:"#fbbe4c",
        padding:12,
        borderRadius:4,
        alignItems:"center",
        marginTop:10,
        flexDirection:'row',
        flexShrink: 1
    },
    buttonText:{
        fontWeight:"300",
        fontSize:13,
        color:"#fff",
        flexWrap: 'wrap',
        flexShrink: 1
    },
    voltar:{
        marginRight:25
    },
    desc:{
        color:"#ddd",
        fontWeight:"300",
        fontSize:12,
    },
    entrar:{
        backgroundColor:'#fbbe4c',
        padding:12,
        borderRadius:4,
        justifyContent:'center',
        alignItems:'center'
    },

    entrarText:{
        color:"#fff",
        fontWeight:"500",
        fontSize:16,
    },
    footer:{
         marginTop:16,
         marginBottom:16
    },

    cardModelo:{
        color:"#FF6B00"
    },

    cardTitle:{
        fontWeight:"bold",
    },
    itemTitle2:{
        fontWeight:"bold",
        paddingHorizontal:22,
        fontSize:12
    },

    itemTitle:{
        fontWeight:"bold",
      
    },
    typesBtn:{
        marginRight:22,
        alignItems:'center'
    },
    types:{
        flexDirection:'row',
        marginTop:12
    },
   
    selects:{
        marginVertical:10
    },
    selectItem:{
        flexDirection:'row',
        marginTop:5
    },
   
    
    
})