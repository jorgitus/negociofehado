import React, { useState } from 'react';

import {
    View,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { CheckBox, BottomSheet, ListItem } from 'react-native-elements'
import { Icon, Input } from 'react-native-elements';
import { RectButton } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import styles from "./styles";
import { DetailCard } from "../../components/Card"
import TopBar from "../../components/TopBar"
import DateTimePicker from '@react-native-community/datetimepicker';
import StylesGlobal from "../../stylesGlobal";



const Detalhe = () => {

    const navigation = useNavigation();

    function handleNavigation() {
        navigation.navigate('Login');
    }

    function handlePublicacao() {
        navigation.navigate('Publicacao');
    }

    function handleNavigationback() {
        navigation.goBack();
    }

    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [maquina, setMaquina] = useState(false);
    const [maquinaLocador, setMaquinaLocador] = useState(false);
    const [location, setLocation] = useState("Av. Frei Cirilo, 314 - Cajazeiras, Fortaleza - CE");


    const [isVisible, setIsVisible] = useState(false);
    const list = [
        { title: 'Av. Frei Cirilo, 314 - Cajazeiras, Fortaleza - CE', onPress: () => (setLocation('Av. Frei Cirilo, 314 - Cajazeiras, Fortaleza - CE'), setIsVisible(false)), },
        { title: 'Av. Francisco Marquês da Fonseca, 484 - Rio do Meio, Bayeux - PB, 58308-000', onPress: () => (setLocation('Av. Francisco Marquês da Fonseca, 484 - Rio do Meio, Bayeux - PB, 58308-000'), setIsVisible(false)) },
        {
            title: 'Cancelar',
            containerStyle: { backgroundColor: 'red' },
            titleStyle: { color: 'white' },
            onPress: () => setIsVisible(false),
        },
    ];


    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };


    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };


    const modifyLocation = () => {
        setIsVisible(true);
    };
    const checkType = (tipo) => {
        if (tipo === 'maquina') {
            setMaquina(true);
            setMaquinaLocador(false)
        } else {
            setMaquina(false);
            setMaquinaLocador(true)
        }
    };


    return (
        <>
            <View style={StylesGlobal.page}>
                <TopBar>
                    <RectButton style={styles.voltar} onPress={handleNavigationback}>
                        <Icon name='arrow-left' type='feather' size={26} color='#222' />
                    </RectButton>
                    <Text style={StylesGlobal.titleStyle}>Orçamento</Text>
                </TopBar>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={StylesGlobal.container}>


                      
                            <View style={styles.details}>
                                <View>
                                    <DetailCard
                                        title="ESCAVADEIRA HIDRÁULICA CX130B"
                                        logoTitle={require('../../assets/case.png')}
                                        fornecedor="Fornecedora Maquinas"
                                        itemImage={require('../../assets/escavadeiraCase.jpg')}
                                        rating={require('../../assets/stars.png')}
                                    />


                                    <View style={styles.descricao}>
                                        <Text>Descrição:</Text>
                                        <Text>Entregamos em toda capital, gratuito.
                                        Equipamento revisado.</Text>
                                    </View>

                                    <View style={styles.item}>
                                        <View style={styles.itemHeader}>
                                            <Icon name='calendar' type='feather' size={26} color='#222' />
                                            <Text style={styles.itemTitle2}>
                                                PERÍODO DE LOCAÇÃO
                                            </Text>
                                        </View>
                                        <View style={styles.content}>
                                            <TouchableOpacity style={styles.button} onPress={showDatepicker}><Text style={styles.buttonText}>{date.toLocaleDateString('pt-BR')}</Text></TouchableOpacity>
                                            <Text>ATÉ</Text>
                                            <TouchableOpacity style={styles.button} onPress={showDatepicker}><Text style={styles.buttonText}>{date.toLocaleDateString('pt-BR')}</Text></TouchableOpacity>

                                        </View>
                                    </View>


                                    <View style={styles.item}>
                                        <View style={styles.itemHeader}>
                                            <Icon name='clock' type='feather' size={26} color='#222' />
                                            <Text style={styles.itemTitle2}>
                                                HORAS DE TRABALHO
                                            </Text>
                                        </View>
                                        <View style={styles.content}>
                                            <TouchableOpacity style={styles.button} onPress={showTimepicker}><Text style={styles.buttonText}> 0 Horas </Text></TouchableOpacity>
                                        </View>
                                    </View>

                                    <View style={styles.item}>
                                        <View style={styles.itemHeader}>
                                            <Icon name='edit' type='feather' size={26} color='#222' />
                                            <Text style={styles.itemTitle2}>
                                                TIPO DE CONTRATO
                                         </Text>
                                        </View>
                                        <View style={styles.content}>
                                            <CheckBox
                                                containerStyle={{ backgroundColor: "transparent", borderColor: "transparent" }}
                                                textStyle={{ fontSize: 12 }}
                                                checkedIcon='dot-circle-o'
                                                uncheckedIcon='circle-o'
                                                title='Maquina'
                                                checkedColor="#ff7e0d"
                                                checked={maquina}
                                                onPress={() => checkType('maquina')}
                                            />

                                            <CheckBox
                                                containerStyle={{ backgroundColor: "transparent", borderColor: "transparent" }}
                                                textStyle={{ fontSize: 12 }}
                                                checkedIcon='dot-circle-o'
                                                uncheckedIcon='circle-o'
                                                title='Maquina e Operador'
                                                checkedColor="#ff7e0d"
                                                checked={maquinaLocador}
                                                onPress={() => checkType('locador')}

                                            />
                                        </View>
                                    </View>
                                    <View style={styles.item}>
                                        <View style={styles.itemHeader}>
                                            <Icon name='map-pin' type='feather' size={26} color='#222' />
                                            <Text style={styles.itemTitle2}>
                                                LOCALIZAÇÃO DE ENTREGA/RETIRADA
                                        </Text>
                                        </View>
                                        <View style={styles.content}>
                                            <TouchableOpacity onPress={modifyLocation} style={styles.button}>
                                                <View style={{ flexDirection: "row", alignItems: "center" }}>
                                                    <Icon name='map-pin' type='feather' size={18} color='#fff' />
                                                    <Text style={styles.headerTextBolder}>Enviando para  {location}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                </View>
                                <View>
                                </View>
                            </View>
                       

                        <View style={styles.footer}>
                            <RectButton style={styles.entrar}>
                                <Text style={styles.entrarText}>Simular</Text>
                            </RectButton>
                        </View>


                    </View>
                </ScrollView>

            </View>

            <BottomSheet isVisible={isVisible}>
                {list.map((l, i) => (
                    <ListItem key={i} containerStyle={l.containerStyle} onPress={l.onPress}>
                        <ListItem.Content>
                            <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                ))}
            </BottomSheet>

            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}

        </>
    )

}

export default Detalhe;


