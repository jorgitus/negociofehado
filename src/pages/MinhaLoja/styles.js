import { StyleSheet } from "react-native";



export default StyleSheet.create({

    container: {
        paddingHorizontal: '6%',
        paddingTop: 25,
        paddingBottom: 27,
        backgroundColor: "#EA5300",
        flex: 1
    },
    menu: {
        width: 50
    },
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginBottom: 7,
    },

    drawer: {
        backgroundColor: "red",
        justifyContent: 'flex-start'
    },
    headerText: {
        fontSize: 15,
        color: "#fff"
    },
    logo: {
        fontSize: 20,
        color: "#fff"
    },
    title: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#ffffff"
    },
    headerTextBolder: {
        fontWeight: "bold",
        color: "#f1f1f1"

    },
    description: {
        fontSize: 16,
        lineHeight: 24,
        color: "#222"
    },
    card: {
        backgroundColor: "#fff",
        marginTop: 15,
        height: 280,
        justifyContent: 'space-between'
    },
    cardHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 22,
        paddingHorizontal: 22,
    },

    cardBodyItem: {
        justifyContent: 'space-between',

    },
    cardBody: {
        paddingVertical: 22,
        paddingHorizontal: 22,
    },

    cardBodySaldo: {
        fontSize: 26
    },

    cardFooter: {
        backgroundColor: "#eee",
        paddingVertical: 22,
        paddingHorizontal: 22,
    },
    cardFooterText: {
        fontSize: 12
    },

    subinfo: {
        color: '#2c2c2c',
        fontSize: 11
    },
    nav: {
        marginBottom: 22,
        marginTop: 15,
    },


    itemsContainer: {
        flexDirection: 'row',
        paddingTop: '6%',
        paddingBottom: '6%',
        paddingLeft: '6%',
        backgroundColor:"#ff6f00"
    },

    item: {
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#eee',
        height: 100,
        width: 100,
        borderRadius: 6,
        paddingHorizontal: 10,
        paddingTop: 20,
        paddingBottom: 16,
        marginRight: 8,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
    },

    item2: {
        backgroundColor: '#fff',
        height: 100,
        width: 100,
        borderRadius: 6,
        paddingHorizontal: 10,
        paddingTop: 20,
        paddingBottom: 16,
        margin: 8,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
    },

    itemTitle: {
        fontSize: 12
    }


})