import React, { Component } from 'react';
import {
    View,
    Button,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    Text

} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import styles from "./styles";
import { useNavigation } from "@react-navigation/native";

export default function Home() {


    const navigation = useNavigation();

    function handleNavigation() {
        navigation.navigate('Solicitacoes');
    }

    function handleChat() {
        navigation.navigate('Chat');
    }

    function handleDrawer() {
        navigation.openDrawer();
    }


    return (
        <>
            <ScrollView>
                <LinearGradient
                    style={styles.container}
                    colors={['#ff8c1f', '#ff8714', '#ff6f00']}>
                    <View>
                        <View style={styles.nav}>
                            <TouchableHighlight style={styles.menu} onPress={handleDrawer}>
                                <Icon name='menu' type='feather' size={36} color='#fff' />
                            </TouchableHighlight>
                        </View>
                        <View style={styles.card}>

                            <View style={styles.cardHeader}>
                                <Icon name='dollar-sign' type='feather' size={26} color='#222' />
                                <Icon name='eye-off' type='feather' size={26} color='#222' />
                            </View>

                            <View style={styles.cardBody}>
                                <Text style={styles.cardFooterText}>Saldo em vendas</Text>
                                <Text style={styles.cardBodySaldo}>R$ 197.611,65</Text>
                            </View>

                            <View style={styles.cardFooter}>

                                <Text style={styles.cardFooterText}>Aluguel de uma miniescavadeira às 15 horas</Text>
                                <Text style={styles.cardFooterText}>Valor de R$ 1.500,00</Text>
                            </View>
                        </View>

                        <View style={styles.card}>
                            <View style={styles.cardHeader}>
                                <View style={styles.cardBodyItem}>
                                    <TouchableOpacity style={styles.item2}>
                                        <Icon name='dollar-sign' type='feather' size={43} color='#222' />
                                        <Text style={styles.itemTitle}>Caixa</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={styles.item2}>
                                        <Icon name='shopping-cart' type='feather' size={43} color='#222' />
                                        <Text style={styles.itemTitle}>Historico</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.cardBodyItem}>
                                    <TouchableOpacity onPress={handleNavigation} style={styles.item2}>
                                        <Icon name='shopping-bag' type='feather' size={43} color='#222' />
                                        <Text style={styles.itemTitle}>Loja</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={handleChat} style={styles.item2}>
                                        <Icon name='message-circle' type='feather' size={43} color='#222' />
                                        <Text style={styles.itemTitle}>Chat</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                       
                    </View>
                </LinearGradient>
            </ScrollView>
            <View style={styles.itemsContainer}>
                            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                <TouchableOpacity style={styles.item}>
                                    <Icon name='star' type='feather' size={43} color='#222' />
                                    <Text style={styles.itemTitle}>Destacar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.item} onPress={handleNavigation}>
                                    <Icon name='bar-chart' type='feather' size={43} color='#222' />
                                    <Text style={styles.itemTitle}>Relatórios</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.item} onPress={() => { }}>
                                    <Icon name='share' type='feather' size={43} color='#222' />
                                    <Text style={styles.itemTitle}>Publicações</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.item} onPress={() => { }}>
                                    <Icon name='smile' type='feather' size={43} color='#222' />
                                    <Text style={styles.itemTitle}>Clientes</Text>
                                </TouchableOpacity>
                            </ScrollView>
                        </View>
        </>
    )
}

