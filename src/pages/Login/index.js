import React, {Component, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Dimensions,
  SafeAreaView,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {RectButton} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import StylesGlobal from '../../stylesGlobal';
import Icon from 'react-native-vector-icons/FontAwesome';
import {CheckBox} from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

import Users from '../../model/users';

const Return = () => {
  const navigation = useNavigation();

  function handleNavigation() {
    navigation.navigate('Localizacao');
  }

  const [rendered, setPage] = useState('login');

  const setPageRendered = (page) => {
    setPage(page);
  };

  const [data, setData] = React.useState({
    username: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
  });

  const textInputChange = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        username: val,
        check_textInputChange: true,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textInputChange: false,
        isValidUser: false,
      });
    }
  };

  const handlePasswordChange = (val) => {
    if (val.trim().length >= 8) {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const handleValidUser = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        isValidUser: false,
      });
    }
  };

  const loginHandle = (userName, password) => {
    const foundUser = Users.filter((item) => {
      return userName == item.username && password == item.password;
    });

    if (data.username.length == 0 || data.password.length == 0) {
      Alert.alert(
        'Wrong Input!',
        'Username or password field cannot be empty.',
        [{text: 'Okay'}],
      );
      return;
    }

    if (foundUser.length == 0) {
      Alert.alert('Invalid User!', 'Username or password is incorrect.', [
        {text: 'Okay'},
      ]);
      return;
    }
  };

  const Login = () => {
    return (
      <>
        <SafeAreaView style={StylesGlobal.page}>
          <View style={styles.container}>
            <View style={styles.header}>
              <Animatable.Image
                animation="bounceIn"
                duraton="500"
                source={require('../../assets/logo2.jpeg')}
                style={styles.logo}
                resizeMode="stretch"
              />
            </View>

            <Animatable.View animation="fadeInUpBig" style={styles.footer}>
              <View style={styles.slogan}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginRight: 12,
                      }}>
                      <Text
                        onPress={() => setPageRendered('login')}
                        style={{
                          fontSize: 14,
                          fontWeight: '700',
                          color: 'black',
                          marginBottom: 3,
                        }}>
                        Entrar
                      </Text>
                      <View
                        style={{
                          width: 5,
                          height: 5,
                          backgroundColor: '#fbbe4c',
                          borderRadius: 7,
                        }}></View>
                    </View>

                    <Text
                      onPress={() => setPageRendered('cadastrar')}
                      style={styles.sloganTextDisabled}>
                      Cadastrar
                    </Text>
                  </View>

                  <TouchableOpacity onPress={updateSecureTextEntry}>
                    {data.secureTextEntry ? (
                      <Feather name="eye-off" color="grey" size={20} />
                    ) : (
                      <Feather name="eye" color="grey" size={20} />
                    )}
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.formGroup}>
                <TextInput
                  placeholder="Email/Usuario"
                  placeholderTextColor="#666666"
                  style={StylesGlobal.inputStyle}
                  autoCapitalize="none"
                  onChangeText={(val) => textInputChange(val)}
                  onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                />

                <TextInput
                  placeholder="Senha"
                  placeholderTextColor="#666666"
                  secureTextEntry={data.secureTextEntry ? true : false}
                  style={StylesGlobal.inputStyle}
                  onChangeText={(val) => handlePasswordChange(val)}
                />
              </View>

              <View>
                <RectButton style={styles.button} onPress={handleNavigation}>
                  <Text style={styles.buttonText}>Entrar</Text>
                </RectButton>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: '100%',
                  }}>
                  <CheckBox
                    containerStyle={{
                      backgroundColor: 'transparent',
                      borderColor: 'transparent',
                      padding: 0,
                      marginLeft: 0,
                    }}
                    textStyle={{fontSize: 12, color: '#CBCBCB'}}
                    title="Manter Conectado"
                    checkedColor="#fbbe4c"
                    checked={true}
                  />
                  <Text style={{color: '#CBCBCB'}}>Esqueceu a senha ?</Text>
                </View>

                {/* <View style={styles.or}>
                <View style={styles.orText}>
                  <Text style={{color: '#CBCBCB'}}>Ou</Text>
                </View>
              </View> */}
                {/* <RectButton style={styles.buttonGoole} onPress={handleNavigation}>
                <Text style={styles.buttonText}>Google</Text>
              </RectButton> */}
              </View>
            </Animatable.View>
          </View>
        </SafeAreaView>
      </>
    );
  };

  const Cadastro = () => {
    return (
      <>
      <SafeAreaView style={StylesGlobal.page}>
          <View style={styles.container}>
            <View style={styles.header}>
              <Animatable.Image
                animation="bounceIn"
                duraton="500"
                source={require('../../assets/logo2.jpeg')}
                style={styles.logo}
                resizeMode="stretch"
              />
            </View>

            <Animatable.View animation="fadeInUpBig" style={styles.footer}>
              <View style={styles.slogan}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                 

                    <Text
                      onPress={() => setPageRendered('login')}
                      style={styles.sloganTextDisabled}>
                      Entrar
                    </Text>

                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginRight: 12,
                      }}>
                      <Text
                        onPress={() => setPageRendered('login')}
                        style={{
                          fontSize: 14,
                          fontWeight: '700',
                          color: 'black',
                          marginBottom: 3,
                        }}>
                        Cadastrar
                      </Text>
                      <View
                        style={{
                          width: 5,
                          height: 5,
                          backgroundColor: '#fbbe4c',
                          borderRadius: 7,
                        }}></View>
                    </View>


                  </View>

                  <TouchableOpacity onPress={updateSecureTextEntry}>
                    {data.secureTextEntry ? (
                      <Feather name="eye-off" color="grey" size={20} />
                    ) : (
                      <Feather name="eye" color="grey" size={20} />
                    )}
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.formGroup}>
                <TextInput
                  placeholder="Email/Usuario"
                  placeholderTextColor="#666666"
                  style={StylesGlobal.inputStyle}
                  autoCapitalize="none"
                  onChangeText={(val) => textInputChange(val)}
                  onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                />

                <TextInput
                  placeholder="Senha"
                  placeholderTextColor="#666666"
                  secureTextEntry={data.secureTextEntry ? true : false}
                  style={StylesGlobal.inputStyle}
                  onChangeText={(val) => handlePasswordChange(val)}
                />

                <TextInput
                  placeholder="Confirmar Senha"
                  placeholderTextColor="#666666"
                  secureTextEntry={data.secureTextEntry ? true : false}
                  style={StylesGlobal.inputStyle}
                  onChangeText={(val) => handlePasswordChange(val)}
                />
              </View>

              <View>
                <RectButton style={styles.button} onPress={handleNavigation}>
                  <Text style={styles.buttonText}>Entrar</Text>
                </RectButton>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: '100%',
                  }}>
                  <CheckBox
                    containerStyle={{
                      backgroundColor: 'transparent',
                      borderColor: 'transparent',
                      padding: 0,
                      marginLeft: 0,
                    }}
                    textStyle={{fontSize: 12, color: '#CBCBCB'}}
                    title="Manter Conectado"
                    checkedColor="#fbbe4c"
                    checked={true}
                  />
                  <Text style={{color: '#CBCBCB'}}>Esqueceu a senha ?</Text>
                </View>

                {/* <View style={styles.or}>
                <View style={styles.orText}>
                  <Text style={{color: '#CBCBCB'}}>Ou</Text>
                </View>
              </View> */}
                {/* <RectButton style={styles.buttonGoole} onPress={handleNavigation}>
                <Text style={styles.buttonText}>Google</Text>
              </RectButton> */}
              </View>
            </Animatable.View>
          </View>
        </SafeAreaView>
      </>
    );
  };
  if (rendered == 'login') {
    return Login();
  } else {
    return Cadastro();
  }
};

export default Return;
