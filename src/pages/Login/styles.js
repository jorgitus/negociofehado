import {StyleSheet} from 'react-native';
import {Dimensions} from 'react-native';

const windowHeight = Dimensions.get('window').height;
const {height} = Dimensions.get('screen');
const height_logo = height * 0.2;
export default StyleSheet.create({
  slogan: {
    justifyContent: 'center',
    // marginTop: Dimensions.get("window").height/2.3,
  },
  sloganText: {
    fontSize: 14,
    fontWeight: '700',
    paddingRight: 12,
    fontFamily: 'Open Sans',
  },
  sloganTextDisabled: {
    fontSize: 14,
    fontWeight: '700',
    paddingRight: 12,
    color: '#CBCBCB',
    padding: 12,
    fontFamily: 'Open Sans',
  },
  container: {
    flex: 1,
    backgroundColor: '#2F3649',
  },
  button: {
    backgroundColor: '#fbbe4c',
    padding: 12,
    borderRadius: 6,
    alignItems: 'center',
    marginVertical: 8,
    width: '100%',
  },
  buttonGoole: {
    backgroundColor: '#EA4335',
    padding: 12,
    borderRadius: 6,
    alignItems: 'center',
    marginVertical: 8,
    width: '100%',
  },
  buttonText: {
    color: '#fff',
    fontWeight: '600',
    fontSize: 15,
    fontFamily: 'Open Sans',
  },
  footer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  or: {
    borderColor: '#CBCBCB',
    borderBottomWidth: 1,
    width: '100%',
    paddingHorizontal: '8%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: Dimensions.get('window').height / 25,
  },
  orText: {
    padding: 22,
    paddingHorizontal: 12,
    marginBottom: -Dimensions.get('window').height / 25,
    backgroundColor: '#FBFBFB',
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  logo: {
    width: height_logo * 1.8,
    height: height_logo,
  },
  footer: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
    fontFamily: 'Open Sans',
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
    fontFamily: 'Open Sans',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  // button: {
  //     alignItems: 'center',
  //     marginTop: 50
  // },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'Open Sans',
  },
});
