import React, { Component, useState, useRef } from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Image,
  Button,
  TextInput
} from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import { Icon, Text } from 'react-native-elements';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';
import TopBar from '../../components/TopBar';
import StylesGlobal from '../../stylesGlobal';
import Animated from 'react-native-reanimated';
import { Modalize } from 'react-native-modalize';
import Carousel, { Pagination } from 'react-native-snap-carousel';

export default function Categorias({ route }) {
  const navigation = useNavigation();


  function handleHome() {
    navigation.navigate('Home');
  }

  const modalizeRef = useRef(null);
  const carousel = useRef('');
  const onOpen = () => {
    modalizeRef.current?.open();
  };


  const onClose = () => {
    modalizeRef.current?.close();
  };


  // const [activeIndex, setActive] = useState(0);
 
  //    const carouselItems = [
  //     {
  //         title:"Item 1",
  //         text: "Text 1",
  //         onOpen:  onOpen()
  //     },
  //     {
  //         title:"Item 2",
  //         text: "Text 2",
  //         onOpen:  onOpen()
  //     },
  //     {
  //         title:"Item 3",
  //         text: "Text 3",
  //         onOpen:  onOpen()
  //     },
  //     {
  //         title:"Item 4",
  //         text: "Text 4",
  //         onOpen:  onOpen()
  //     },
  //     {
  //         title:"Item 5",
  //         text: "Text 5",
  //         onOpen: onOpen()
  //     },
  //   ]
  


// const _renderItem= ({item,index}) => {
//     return (
//       <View onPress={() => item.onOpen} style={{
//           backgroundColor:'floralwhite',
//           borderRadius: 5,
//           height: 250,
//           padding: 50,
//           marginLeft: 10,
//           marginRight: 10, }}>
//         <Text style={{fontSize: 30}}>{item.title}</Text>
//         <Text>{item.text}</Text>
//       </View>

//     )
// }

  return (
    <>
      <SafeAreaView style={StylesGlobal.page}>
        <TopBar>
          <TouchableOpacity style={styles.voltar} onPress={handleHome}>
            <Icon name="arrow-left" type="feather" size={26} color="#182133" />
          </TouchableOpacity>

          <Text style={StylesGlobal.text}>Inserir Publicacão</Text>
        </TopBar>




        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={StylesGlobal.container}>

          <RectButton style={styles.inputCamera} onPress={onOpen}>
            <Icon name="camera" type="feather" size={46} color="#ffd078" />
          </RectButton>
          
          {/* <Carousel
                  layout={"default"}
                  ref={carousel}
                  data={carouselItems}
                  sliderWidth={300}
                  itemWidth={300}
                  renderItem={_renderItem}
                  onSnapToItem = { index => setActive(index)} /> */}




          <View style={styles.body}>
            <View>
              <Text>Titulo</Text>
              <TextInput
                placeholder="Ex: Escavadeira Hidráulica"
                placeholderTextColor="#666666"
                style={StylesGlobal.inputStyle}
              />
            </View>
           


            <View>
              <Text>Descrição</Text>
              <TextInput
                placeholder="Ex: Motor de grande eficiência, elevado torque e baixo consumo."
                placeholderTextColor="#666666"
                multiline={true}
                style={StylesGlobal.textArea}

              />
            </View>

            <View>
              <Text>Categoria</Text>
              <TextInput
                placeholder="Ex: Maquinas"
                placeholderTextColor="#666666"
                style={StylesGlobal.inputStyle}
              />
            </View>
          </View>


          <RectButton style={styles.button}>
            <Text style={styles.buttonText}>Salvar</Text>
          </RectButton>
        </ScrollView>
        <Modalize snapPoint={220} ref={modalizeRef}>
          <View style={styles.panel}>

            <TouchableOpacity style={styles.panelButton}>
              <Icon
                name='camera'
                type='feather'
                color='#222'
              />
              <Text style={styles.panelButtonTitle}>Tirar foto</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.panelButton}>
              <Icon
                name='file'
                type='feather'
                color='#222'
              />
              <Text style={styles.panelButtonTitle}>Escolher da galeria</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.panelButton}
              onPress={onClose}>
              <Icon
                name='x'
                type='feather'
                color='#222'
              />
              <Text style={styles.panelButtonTitle}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </Modalize>
      </SafeAreaView>
    </>
  );
}


