import { StyleSheet } from "react-native";
import { Dimensions } from 'react-native';


const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;



export default StyleSheet.create({

  voltar: {
    marginRight: 25
  },
  itemsContainer: {
    flexDirection: 'row',
    marginTop: 16,
    marginBottom: 22,
  
  },
  body:{
    paddingVertical:30
  },


  item: {
    height: windowWidth/2.2,
    width: windowWidth/2.2,
    borderRadius: 6,
    paddingHorizontal: 2,
    paddingTop: 20,
    paddingBottom: 16,
    margin: 6,
    alignItems: 'center',
    justifyContent: 'space-around',
    textAlign: 'center',
    backgroundColor: "#FFF",
    shadowColor: "#3F52F2",
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,
    elevation: 3
  },

  itemTitle: {
    textAlign: 'center',
    fontSize: 14.5,
    color: "#182133",
    fontFamily: "Roboto"
  },

  categorias:{
      width:'100%',
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop:windowHeight/100,
     
  },


  inputCamera :{
      width: '100%',
      height:windowHeight/5,
      backgroundColor:"#fff",
      justifyContent:'center',
      borderWidth:12,
      borderColor:'#ffd078',
      borderRadius:8
  },
  commandButton: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginTop: 10,
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    // borderTopLeftRadius: 20,
    // borderTopRightRadius: 20,
    // shadowColor: '#000000',
    // shadowOffset: {width: 0, height: 0},
    // shadowRadius: 5,
    // shadowOpacity: 0.4,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    // elevation: 5,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 6,
    borderRadius: 3,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    marginVertical: 7,
    flexDirection:'row'
  },
  panelButtonTitle: {
    fontSize: 16,
    color: '#222',
    paddingHorizontal:12

  },
  button: {
    backgroundColor: '#fbbe4c',
    padding: 12,
    borderRadius: 6,
    alignItems: 'center',
    width: '100%',
  },
  buttonText: {
    color: '#fff',
    fontWeight: '600',
    fontSize: 15,
    fontFamily: 'Open Sans',
  },

})