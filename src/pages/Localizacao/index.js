import React, { Component } from 'react';
import {
  View,
  Text,
  ImageBackground
} from 'react-native';
import { Icon,Input } from 'react-native-elements';
import {RectButton} from "react-native-gesture-handler";
import {useNavigation} from "@react-navigation/native";
import styles from "./styles";

const Localizacao = () => {
    
        const navigation = useNavigation();
    
        function handleNavigation(){
            navigation.navigate('Home');
        }
   
        return (
            
            <>
               <ImageBackground source={require('../../assets/background2.png')} style={styles.container} >

                    <View style={styles.title}>
                        <Text style={styles.title}>Minha Localização</Text>
                    </View>

                    <Input
                    placeholder='Buscar endereço e numero'
                    leftIcon={
                        <Icon name='search' type='feather' size={18} color='#222'/>
                    }
                    />

                    <RectButton style={styles.button}>
                        <Icon name='map-pin' type='feather' style={{marginRight:15}} size={18} color='#222'/>
                        <View>
                            <Text style={styles.buttonText}>Usar Localizacao atual</Text>
                            <Text style={styles.desc}>Ativar Localizacao</Text>
                        </View>
                    </RectButton>

                    <View style={styles.footer}>
                        <RectButton style={styles.entrar} onPress={handleNavigation}>
                            <Text style={styles.entrarText}>Pesquisar</Text>
                        </RectButton>
                    </View>

                
                </ImageBackground>
            </>
        )
    
}

export default Localizacao;


