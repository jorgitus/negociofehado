import {StyleSheet} from "react-native";



export default StyleSheet.create({

    container:{
        paddingHorizontal:24,
        paddingTop:25,
        paddingBottom:27,
        backgroundColor:"#ecf1f8",
       
        flex:1
    },
    header:{
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center",
        marginBottom:7,
    },
    headerText:{
        fontSize:15,
        color: "#fff"
    },
    logo:{
        fontSize:20,
        color: "#fff"
    },
    title:{
        fontSize:22,
        fontWeight:"500",
        color:"#222",
        marginTop:100,
    },
    headerTextBolder:{
       fontWeight:"bold",
       color:"#f1f1f1"

    },
    description:{
        fontSize:16,
        lineHeight:24,
        color:"#222"
    },
    card:{
        paddingVertical:22,
        backgroundColor:"#fff",
        marginTop:15,
        borderRadius:8,
        padding:12
        
    },
    button:{
        backgroundColor:"#fff",
        padding:12,
        borderRadius:6,
        alignItems:"center",
        marginTop:10,
        flexDirection:'row'
    },
    buttonText:{
        color:"#222",
        fontWeight:"300",
        fontSize:16,
       
    },
    desc:{
        color:"#ddd",
        fontWeight:"300",
        fontSize:12,
    },
    entrar:{
        backgroundColor:'#FCB031',
        padding:12,
        borderRadius:4,
        justifyContent:'center',
        alignItems:'center'
    },
    entrarText:{
        color:"#fff",
        fontWeight:"500",
        fontSize:16,
    },
    footer:{
         marginTop:16
    }
    
    
})