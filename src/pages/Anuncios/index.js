import React, { Component } from 'react';
import {
    View,
    Image,
    FlatList,
    StyleSheet,
    ScrollView,
    ImageBackground,
    TouchableOpacity,
    TextInput,
    Text
} from 'react-native';
// import { Text } from 'native-base';
import { Icon, Input } from 'react-native-elements';

import styles from "./styles";
import { useNavigation } from "@react-navigation/native";







export default function Home() {


    const navigation = useNavigation();

    function handleNavigation() {
        navigation.navigate('Categoria');
    }

    return (
        <>
            <ScrollView>
                <View style={styles.container}>

                    <View style={styles.nav}>
                        {/* <View style={styles.header}>
                        <Text style={styles.logo}>Logo</Text>
                         <Text style={styles.headerText}>Voce tem <Text style={styles.headerTextBolder}>18 filhos</Text></Text> 
                    </View> */}


                        <Text style={styles.title}>Anuncios</Text>

                        <View style={styles.filtroComponent}>
                            {/* <Text style={styles.searchTitle}>Pesquisar</Text>
                        <TextInput placeholder="Busque por ex. Escavadeira" style={{ marginTop:12, height: 50,paddingHorizontal:12, borderColor: '#ccc',borderRadius:4, borderWidth: 1,backgroundColor:"#fff"}}/> */}

                            <TouchableOpacity style={styles.filtro} onPress={() => { }}>

                                <Text style={styles.filtroTitle}>Filtros</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.filtro} onPress={() => { }}>

                                <Text style={styles.filtroTitle}>Tipo de Serviço</Text>
                            </TouchableOpacity>


                            <TouchableOpacity style={styles.filtro} onPress={() => { }}>

                                <Text style={styles.filtroTitle}>Ordenar</Text>
                            </TouchableOpacity>
                        </View>

                    </View>






                    <View style={styles.search}>
                        <Text style={styles.searchTitle}>Pesquisar</Text>
                        <Input
                            placeholder='Buscar endereço e numero'
                            leftIcon={
                                <Icon name='search' type='feather' size={18} color='#222' />
                            }
                        />
                    </View>

                    <Text style={styles.searchTitle}>Meus Anuncios</Text>

                    <TouchableOpacity style={styles.card} onPress={() => { }}>

                        <View style={styles.cardLogo}>
                            <Image style={{ width: 120, height: 45 }} source={require('../../assets/fornecedora.png')} />
                        </View>

                        <View style={styles.infos}>
                            <Text style={styles.infotitle}>Tração completa cx220 </Text>

                            <View style={styles.range}>
                                <Text style={{ fontSize: 12 }}>Fortaleza-CE</Text>
                                <Text style={{ fontSize: 12 }}>R$  230,00</Text>
                            </View>


                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.card} onPress={() => { }}>

                        <View style={styles.cardLogo}>
                            <Image style={{ width: 120, height: 45 }} source={require('../../assets/fornecedora.png')} />
                        </View>

                        <View style={styles.infos}>
                            <Text style={styles.infotitle}>Tração completa cx220 </Text>

                            <View style={styles.range}>
                                <Text style={{ fontSize: 12 }}>Fortaleza-CE</Text>
                                <Text style={{ fontSize: 12 }}>R$  230,00</Text>
                            </View>


                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.card} onPress={() => { }}>

                        <View style={styles.cardLogo}>
                            <Image style={{ width: 120, height: 45 }} source={require('../../assets/fornecedora.png')} />
                        </View>

                        <View style={styles.infos}>
                            <Text style={styles.infotitle}>Tração completa cx220 </Text>

                            <View style={styles.range}>
                                <Text style={{ fontSize: 12 }}>Fortaleza-CE</Text>
                                <Text style={{ fontSize: 12 }}>R$  230,00</Text>
                            </View>


                        </View>
                    </TouchableOpacity>



                    <TouchableOpacity style={styles.card} onPress={() => { }}>

                        <View style={styles.cardLogo}>
                            <Image style={{ width: 120, height: 45 }} source={require('../../assets/fornecedora.png')} />
                        </View>

                        <View style={styles.infos}>
                            <Text style={styles.infotitle}>Tração completa cx220 </Text>

                            <View style={styles.range}>
                                <Text style={{ fontSize: 12 }}>Fortaleza-CE</Text>
                                <Text style={{ fontSize: 12 }}>R$  230,00</Text>
                            </View>


                        </View>
                    </TouchableOpacity>



                </View>
            </ScrollView>

        </>
    )
}

