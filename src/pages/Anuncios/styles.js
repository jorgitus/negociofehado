import {StyleSheet} from "react-native";



export default StyleSheet.create({

    container:{
        paddingHorizontal:24,
        paddingTop:25,
        paddingBottom:27,
        backgroundColor:"#ecf1f8",
        flex:1

        
    },
    header:{
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center",
        marginBottom:7,
    },
    headerText:{
        fontSize:15,
        color: "#fff"
    },
    logo:{
        fontSize:20,
        color: "#fff"
    },
    title:{
        fontSize:18,
        fontWeight:"bold",
        color:"#222",
        paddingVertical:12
    },
    headerTextBolder:{
       fontWeight:"bold",
       color:"#f1f1f1"

    },
    description:{
        fontSize:16,
        lineHeight:24,
        color:"#222"
    },
    card:{
        paddingVertical:42,
        paddingHorizontal:22,

        backgroundColor:"#fff",
        marginTop:15,
        borderRadius:8,
   
        flexDirection:"row"
        
    },
    infos:{
   
      paddingHorizontal:22,
      justifyContent:'space-between'
      
    },
    range:{
      flexDirection:"row",
      alignContent:"center",
      justifyContent:'space-between',
      
      
    },
    subinfo:{
      color:'#2c2c2c',
      fontSize:11
    },
    nav:{
        marginBottom:22,
        marginTop:15,
        justifyContent:'center',
        alignItems:'center'

    },
    flatlist:{
        height:300,
        marginBottom:12
    },
    mapa:{
      paddingVertical:22,
      borderRadius:8,
      height:300
    },
   
    
    
      description: {
        color: '#f1f1f1',
        fontSize: 16,
        marginTop: 4,
      },

      search:{
        marginTop:8
      },

      searchTitle:{
        fontSize:16,
        fontWeight:"bold",
        color:"#222"
      },
    
    
  
    
      itemsContainer: {
        flexDirection: 'row',
        marginTop: 16,
        marginBottom: 22,
      },
    
      item: {
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#eee',
        height: 100,
        width: 100,
        borderRadius: 6,
        paddingHorizontal: 10,
        paddingTop: 20,
        paddingBottom: 16,
        marginRight: 8,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
      },
    
      selectedItem: {
        borderColor: '#34CB79',
        borderWidth: 2,
      },
    
      itemTitle: {
        textAlign: 'center',
        fontSize: 10.5
      },

      filtroComponent:{
        marginTop:0,
        flexDirection: 'row',
      },

     
      filtroTitle:{
      
        textAlign: 'center',
        fontSize: 11,
        fontWeight:'bold',
        paddingHorizontal:10,
      },
      filtro: {
        backgroundColor: '#fff',
        height: 40,
        width: 100,
      
        marginRight: 8,
        alignItems: 'center',
        justifyContent:'center',
        textAlign: 'center',
        borderRadius:30
    },
})