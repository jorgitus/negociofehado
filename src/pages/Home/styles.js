import {StyleSheet} from 'react-native';

import { Dimensions } from 'react-native';


const windowHeight = Dimensions.get('window').height;

export default StyleSheet.create({
  headerText: {
    fontSize: 24,
    fontFamily: 'Open Sans SemiBold',
    color: '#fff',
  },

  itemsContainer: {
    flexDirection: 'row',
    marginTop: 16,
    marginBottom: 22,
  },

  background: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height / 2.3,
    backgroundColor: '#fbbe4c',

  },
  item: {
    height: 95,
    width: 105,
    borderRadius: 6,
    paddingHorizontal: 2,
    paddingTop: 15,
    paddingBottom: 16,
    margin: 6,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'center',
    backgroundColor: '#FFF',
    shadowColor: '#3F52F2',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,
    elevation: 3,
  },

  itemTitle: {
    textAlign: 'center',
    fontSize: 12.5,
    paddingTop:3,
    color: '#2F3649',
    fontFamily: 'Open Sans Bold',

  },

  categorias: {
    padding: '4%',
    width:'100%'
   
  },

  text: {
    fontSize: 18,
    color: '#182133',
    fontFamily: 'Open Sans',
    marginLeft: '4%',
  },

  imageItem: {
    width: 52,
    height: 52,
  },
});
