import React, {Component} from 'react';
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  ImageBackground,
  FlatList,
} from 'react-native';

import {Icon, Text, SearchBar} from 'react-native-elements';

import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import Header from '../../components/Header';
import {Card} from '../../components/Card';
import StylesGlobal from '../../stylesGlobal';

export default function Home() {
  const navigation = useNavigation();

  function handleNavigation(categoria) {
    navigation.navigate('Categoria', {
      categoria: categoria,
    });
  }

  function handleFornecedor() {
    navigation.navigate('Fornecedor');
  }

  function handleDrawer() {
    navigation.openDrawer();
  }



  return (
    <>
      <SafeAreaView style={StylesGlobal.page}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <ImageBackground
            style={styles.background}
            source={require('../../assets/homebackground.jpeg')}>
            <Header>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={handleDrawer}>
                  <Icon name="menu" type="feather" size={37} color="#fff" />
                </TouchableOpacity>
              </View>
              <View>
                <Text style={styles.headerText}>Bem-Vindo</Text>
                <Text style={styles.headerText}>
                  O que você está procurando ?
                </Text>
              </View>
              <SearchBar
                containerStyle={{
                  width: '100%',
                  backgroundColor: 'transparent',
                  borderBottomColor: 'transparent',
                  borderTopColor: 'transparent',
                  paddingHorizontal: 0,
                }}
                placeholder="Buscar ..."
                inputStyle={{backgroundColor: '#fff'}}
                inputContainerStyle={{
                  backgroundColor: '#fff',
                  shadowColor: '#3F52F2',
                  shadowOffset: {
                    width: 0,
                    height: 8,
                  },
                  shadowOpacity: 0.46,
                  shadowRadius: 11.14,
                  elevation: 8,
                }}
              />
            </Header>
          </ImageBackground>
   
            {/* <Text style={styles.text}>Categorias</Text> */}
            <FlatList
              style={styles.categorias}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={[
                {key: 1,
                  nome: 'Caminhoes',
                  img: require('../../assets/icons/caminhao.png'),
                  handleNavigation: () => handleNavigation('Caminhões'),
                },
                {
                  key: 2,
                  nome: 'Maquinas',
                  img: require('../../assets/icons/empilhadeira.png'),
                  handleNavigation: () => handleNavigation('Maquinas'),
                },
                {
                  key: 3,
                  nome: 'Operadores',
                  img: require('../../assets/icons/operador.png'),
                  handleNavigation: () => handleNavigation('Operadores'),
                },
                {
                  key: 4,
                  nome: 'Peças',
                  img: require('../../assets/icons/engrenagem.png'),
                  handleNavigation: () => handleNavigation('Peças'),
                },
              ]}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={styles.item}
                  onPress={() => item.handleNavigation()}>
                  <Image style={styles.imageItem} source={item.img} />
                  <Text style={styles.itemTitle}>{item.nome}</Text>
                </TouchableOpacity>
              )}
            />
       
          <View style={StylesGlobal.container}>
            <Text style={StylesGlobal.text}>Os melhores</Text>

            <FlatList
           
              data={[
                {key: 5,nome: 'Fornecedora Maquinas e Equipamentos'},
                {key: 6,nome: 'Fornecedora Maquinas e Equipamentos'},
                {key: 7,nome: 'Fornecedora Maquinas e Equipamentos'},
                {key: 8,nome: 'Fornecedora Maquinas e Equipamentos'},
              ]}
              renderItem={({item}) => (
                <Card
                  rating="4.2"
                  title={item.nome}
                  description="Equipamento, peças"
                  image={require('../../assets/fornecedora.png')}
                  onPress={() => (handleFornecedor())}
                />
              )}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}
