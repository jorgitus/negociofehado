import React, { Component } from 'react';
import {
    View,
    Image,
    TouchableOpacity,
} from 'react-native';
import styles from "./styles";
import cardProduct from "./cardProductStyle";
import CardSolic from "./cardSolicitacao";
import { Icon, Text } from 'react-native-elements';



export class Card extends Component {
    render() {
        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.card} onPress={() => this.props.onPress()}>
                <View style={styles.cardLogo}>
                    <Image style={{ width: 115, height: 45 }} source={this.props.image} />
                </View>
                <View style={styles.infos}>
                    <Text style={styles.infotitle}>{this.props.title}</Text>
                    <View style={styles.range}>
                        <Image style={{ width: 22, height: 22 }} source={require('../../assets/stars.png')} />
                        <Text style={{ color: '#FFBB54', fontSize: 16 }}>{this.props.rating}</Text>
                    </View>
                    <Text style={styles.subinfo}>
                        {this.props.description}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
}

export class CardSolicitacao extends Component {
    render() {
        return (
            <View style={CardSolic.card} onPress={() => { }}>
                <View style={CardSolic.cardContent}>
                    <View style={CardSolic.cardLogo}>
                        <Image style={{ width: 115, height: 100 }} source={this.props.image} />
                    </View>
                    <View style={CardSolic.infos}>
                        <View>
                            <Text style={CardSolic.infotitle}>{this.props.title}</Text>
                            <Text style={CardSolic.periodo}>
                                {this.props.periodo}
                            </Text>
                        </View>

                        <Text style={CardSolic.subinfo}>
                            {this.props.description}
                        </Text>


                    </View>
                </View>

                {this.props.actions ?  <View style={{ flexDirection: 'row',justifyContent:"flex-end",padding:12 }}>

<TouchableOpacity style={CardSolic.buttonReprove} onPress={() => { }}>
        <Text style={{ color: 'red', fontSize: 16 }}>
            Reprovar
        </Text>
    </TouchableOpacity>
    
    <TouchableOpacity style={CardSolic.buttonAprove} onPress={() => { }}>
        <Text style={{ color: '#FFF', fontSize: 16 }}>
            Aprovar
        </Text>
    </TouchableOpacity>

 

</View> : <View style={{ flexDirection: 'row',justifyContent:"flex-end",padding:12 }}>


 

</View>}
               

            </View>
        )
    }
}


export class CardProduct extends Component {
    render() {
        return (
            <View style={cardProduct.card} onPress={() => { }}>
                <>
                <View style={cardProduct.cardHeader}>
                    <Image style={{ width: 80, height: 54, marginRight: 26 }} source={this.props.logoTitle} />
                    <Text style={cardProduct.infotitle}>{this.props.title}</Text>
                </View>


                <View style={cardProduct.cardBody}>
                    <View style={cardProduct.cardBodyData}>
                        <Image style={cardProduct.cardBodyImage} source={this.props.itemImage} />
                     
                    </View>

                    <View style={cardProduct.infos}>
                    <Text style={{ fontSize: 11 }}>{this.props.fornecedor}</Text>
                        <Image style={{ width: 100, height: 22 }} source={this.props.rating} />
                        {/* <View style={cardProduct.selectItem}>
                            <Image style={{ width: 14, height: 18, marginRight: 5 }} source={require('../../assets/checkbox.png')} />
                            <Text style={{ fontSize: 11 }}>Sem taxas de Alteração</Text>
                        </View>

                        <View style={cardProduct.selectItem}>
                            <Image style={{ width: 14, height: 18, marginRight: 5 }} source={require('../../assets/checkbox.png')} />
                            <Text style={{ fontSize: 11 }}>Taxas Administrativas</Text>
                        </View>
                        <View style={cardProduct.selectItem}>
                            <Image style={{ width: 14, height: 18, marginRight: 5 }} source={require('../../assets/checkbox.png')} />
                            <Text style={{ fontSize: 11 }}>Garantia</Text>
                        </View> */}
{/* 
                        <View style={cardProduct.types}>
                            <TouchableOpacity style={cardProduct.typesBtn} onPress={() => { }}>
                                <Image style={cardProduct.cardBodyImageOptions} source={require('../../assets/automatico.jpeg')} />
                                <Text style={cardProduct.itemTitle}>Automatico</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={cardProduct.typesBtn} onPress={() => { }}>
                                <Image style={cardProduct.cardBodyImageOptions} source={require('../../assets/manual.jpeg')} />
                                <Text style={cardProduct.itemTitle}>Hidraulico</Text>
                            </TouchableOpacity>
                        </View> */}
                        <View style={cardProduct.generate}>
                            <TouchableOpacity onPress={this.props.detail} style={cardProduct.actionDetails} >
                                <Icon name='search' type='feather' size={14} color='#F1F1F1' />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.props.orcamento} style={cardProduct.actionCotar}>
                                <Text style={cardProduct.btnText}>Orçamento</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                </>
            </View>
        )
    }
}

export class DetailCard extends Component {
    render() {
        return (
            <View style={cardProduct.card} onPress={() => { }}>
                <View style={cardProduct.cardHeader}>
                    <Image style={{ width: 80, height: 54, marginRight: 12 }} source={this.props.logoTitle} />
                    <Text style={cardProduct.infotitle}>{this.props.title}</Text>
                </View>


                <View style={cardProduct.cardBody}>
                    <View style={cardProduct.cardBodyData}>
                        <Image style={cardProduct.cardBodyImage} source={this.props.itemImage} />
                        <Text style={{ fontSize: 11 }}>{this.props.fornecedor}</Text>
                        <Image style={{ width: 100, height: 22 }} source={this.props.rating} />
                    </View>

                    <View style={cardProduct.infos}>
                        <View style={cardProduct.selectItem}>
                            <Image style={{ width: 14, height: 18, marginRight: 5 }} source={require('../../assets/checkbox.png')} />
                            <Text style={{ fontSize: 11 }}>Sem taxas de Alteração</Text>
                        </View>

                        <View style={cardProduct.selectItem}>
                            <Image style={{ width: 14, height: 18, marginRight: 5 }} source={require('../../assets/checkbox.png')} />
                            <Text style={{ fontSize: 11 }}>Taxas Administrativas</Text>
                        </View>
                        <View style={cardProduct.selectItem}>
                            <Image style={{ width: 14, height: 18, marginRight: 5 }} source={require('../../assets/checkbox.png')} />
                            <Text style={{ fontSize: 11 }}>Garantia</Text>
                        </View>

                        <View style={cardProduct.types}>
                            <TouchableOpacity style={cardProduct.typesBtn} onPress={() => { }}>
                                <Image style={cardProduct.cardBodyImageOptions} source={require('../../assets/automatico.jpeg')} />
                                <Text style={cardProduct.itemTitle}>Automatico</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={cardProduct.typesBtn} onPress={() => { }}>
                                <Image style={cardProduct.cardBodyImageOptions} source={require('../../assets/manual.jpeg')} />
                                <Text style={cardProduct.itemTitle}>Hidraulico</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}


