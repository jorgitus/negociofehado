import { StyleSheet } from "react-native";
import { Dimensions } from 'react-native';

const win = Dimensions.get('window');
const ratio = win.width / 541; //541 is actual image width

export default StyleSheet.create({

  cardLogo: {
    alignItems: "center",
    justifyContent: "center"
  },
  card: {
    paddingVertical: 22,
    paddingHorizontal: 12,
    justifyContent: "flex-start",
    backgroundColor: "#fff",
    marginTop: 12,
    marginHorizontal: 3,

    borderRadius: 8,
    flexDirection: "row",
    backgroundColor: "#FFF",
    shadowColor: "#3F52F2",
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,
    elevation: 3
  },
  infos: {
   
    padding: 10,
    paddingHorizontal:26,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flexWrap: 'wrap',
    flexShrink: 1
  },
  range: {
    flexDirection: "row",
    alignContent: "center"
  },
  subinfo: {
    color: '#2c2c2c',
    fontSize: 13,
    fontFamily: 'Open Sans',
    flexWrap: 'wrap',
    flexShrink: 1

  },
  infotitle: {
    fontFamily: 'Open Sans',
    fontSize:15,
    color:"#222",
    flexWrap: 'wrap',
    flexShrink: 1
  }
})