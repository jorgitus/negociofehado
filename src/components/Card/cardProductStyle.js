import { StyleSheet } from "react-native";
import { Dimensions } from 'react-native';


const win = Dimensions.get('window');
const ratio = win.width / 541; //541 is actual image width

export default StyleSheet.create({

  cardLogo: {
    alignItems: "center",
    justifyContent: "center"
  },
  cardHeader: {
    flexDirection: "row",
    alignItems: "center"

  },
  cardBody: {
    flexDirection: "row",

  },
  card: {
    paddingVertical: 22,
    paddingHorizontal: "6%",
    justifyContent: "center",
    marginTop: 12,
    borderRadius: 8,
    borderColor:"#e1e1e1",
    borderWidth:1
  

  },

  cardBodyImage: {

    width: win.width / 3.5,
    height: 140 * ratio, //362 is actual height of image

  },


  cardBodyImageOptions: {

    width: win.width / 10,
    height: 50 * ratio,
  },
  infotitle: {
    fontSize: 15,
    flexWrap: 'wrap',
    flexShrink: 1
  },
  infos: {

    marginLeft: "5%",
    paddingRight: 30,
    alignItems: "flex-start",
    justifyContent: "flex-start",

  },
  range: {
    flexDirection: "row",
    alignContent: "center"

  },
  subinfo: {
    color: '#2c2c2c',
    fontSize: 11
  },
  types: {
    flexDirection: 'row'
  },


  typesBtn: {
    backgroundColor: '#fff',
    height: 40,
    width: 80,
    paddingTop: 20,
    paddingBottom: 16,
    marginRight: 8,
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'center',
  },

  actionDetails: {
    backgroundColor: "#222",
    padding: 12,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardBodyData: {
    justifyContent: 'space-between'
  },
  generate: {
    flexDirection: 'row',
    marginTop: "12%"
  },
  actionCotar: {
    backgroundColor: "#fbbe4c",
    marginLeft: 8,
    borderRadius: 4,
    width: '65%',
    padding: 2,
    paddingHorizontal: 6,
    justifyContent: "center",
    alignItems: 'center'
  },
  btnText: {
    color: 'white',
    fontSize: 14
  },
  selects: {
    marginVertical: 10
  },
  selectItem: {
    flexDirection: 'row'
  },


  itemTitle: {

    textAlign: 'center',
    fontSize: 12.5,
    color: "#000"

  },


})