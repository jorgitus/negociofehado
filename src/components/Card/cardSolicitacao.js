



import { StyleSheet } from "react-native";
import { Dimensions } from 'react-native';

const win = Dimensions.get('window');
const ratio = win.width / 541; //541 is actual image width

export default StyleSheet.create({

  cardLogo: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: "7%"
  },
  card: {

    backgroundColor: "#fff",
    borderRadius: 8,
    marginTop: "4%",


  },
  cardContent: {
    paddingHorizontal: "6%",
    justifyContent: "flex-start",
    backgroundColor: "#fff",
    marginTop: 12,
    flexDirection: "row"
  },
  infos: {
    marginHorizontal: "12%",
    paddingRight: 70,
    alignItems: "flex-start",
    justifyContent: "space-between",
  },
  range: {
    flexDirection: "row",
    alignContent: "center"
  },
  subinfo: {
    color: '#000',
    fontSize: 13
  },
  infotitle: {
    fontSize: 14,
    paddingTop: "10%"
  },
  periodo: {
    fontSize: 12,
    color: "#5472d1"
  },
  buttonReprove: {
    padding: 12,
    color: "white",
    marginHorizontal: 5,
    width: "30%",
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5

  },
  buttonAprove: {
    padding: 12,
    backgroundColor: "#6ed473",
    color: "white",
    marginHorizontal: 5,
    width: "30%",
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5




  }
})