import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg viewBox="0 0 1440 320" {...props}>
      <Path
        fill="#f50"
        d="M0 288h26.7c26.6 0 80.3 0 133.3-26.7C213.3 235 267 181 320 176c53.3-5 107 37 160 64 53.3 27 107 37 160 10.7 53.3-26.7 107-90.7 160-96C853.3 149 907 203 960 208c53.3 5 107-37 160-26.7 53.3 10.7 107 74.7 160 96 53.3 21.7 107-.3 133-10.6l27-10.7V0H0z"
      />
    </Svg>
  )
}

export default SvgComponent
