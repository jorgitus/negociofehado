import React from 'react';
import { View, StyleSheet } from 'react-native';

import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';
import LinearGradient from 'react-native-linear-gradient';

import { Icon, Text} from 'react-native-elements';


export function DrawerContent(props) {


    return (
        <LinearGradient
            style={{ flex: 1 }}
            colors={['#fff', '#f2f2f2', '#fcfcfc']}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                            {/* <Avatar.Image 
                                source={{
                                    uri: 'https://api.adorable.io/avatars/56'
                                }}
                                size={50}
                            /> */}
                            <View style={{ marginLeft: 15, flexDirection: 'column' }}>
                                <Text style={styles.title}>Negocio Fechado</Text>
                                {/* <Text style={styles.caption}>@j_doe</Text> */}
                            </View>
                        </View>

                        {/* <View style={styles.row}>
                            <View style={styles.section}>
                                <Text style={[styles.paragraph, styles.caption]}>80</Text>
                                <Text style={styles.caption}>Following</Text>
                            </View>
                            <View style={styles.section}>
                                <Text style={[styles.paragraph, styles.caption]}>100</Text>
                                <Text style={styles.caption}>Followers</Text>
                            </View>
                        </View> */}
                    </View>

                    <View style={styles.drawerSection}>
                        <DrawerItem

                            icon={({ color, size }) => (
                                <Icon name='home' type='feather' size={22} color='#222' />
                            )}
                            label="Inicio"
                            onPress={() => { props.navigation.navigate('Home') }}
                        />
                        
                        <DrawerItem
                            icon={({ color, size }) => (
                                <Icon name='briefcase' type='feather' size={22} color='#222' />
                            )}
                            label="Minha Loja"
                            onPress={() => { props.navigation.navigate('MinhaLoja') }}
                        />
                           <DrawerItem
                            icon={({ color, size }) => (
                                <Icon name='message-circle' type='feather' size={22} color='#222' />
                            )}
                            label="Chat"
                            onPress={() => { props.navigation.navigate('Chat') }}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <Icon name='shopping-bag' type='feather' size={22} color='#222' />
                            )}
                            label="Inserir Publicação"
                            onPress={() => { props.navigation.navigate('InserirPublicacao') }}
                        />
                        


                    </View>

                </View>
            </DrawerContentScrollView>
            <View style={styles.bottomDrawerSection}>
                <DrawerItem
                    icon={({color, size}) => (
                        <Icon name='log-out' type='feather' size={22} color='#222' />
                    )}
                    label="Sair"
                    onPress={() => {  props.navigation.navigate('Login')  }}
                />
            </View>
        </LinearGradient>

    );
}

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 8,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 2
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
});
