
import { StyleSheet } from "react-native";
import { Dimensions } from 'react-native';

const win = Dimensions.get('window');

export default StyleSheet.create({
    header: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "transparent",
        paddingHorizontal: "6%",
        paddingTop: "15%",
        paddingBottom: "4%",
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        // shadowOpacity: 0.23,
        // shadowRadius: 2.62,
        // elevation: 4,
    },
    headerText: {
        fontSize: 15,
        color: "#fff"
    }
})