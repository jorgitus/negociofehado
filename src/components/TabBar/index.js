import * as React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

export function MyTabBar({ state, descriptors, navigation }) {
    return (
        <View style={{
            flexDirection: 'row', backgroundColor: "#ffffff", shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 3,
            },
            shadowOpacity: 0.30,
            shadowRadius: 4.65,
            elevation: 7,
        }}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                let iconName;

                switch (route.name) {
                    case 'Inicio':
                        iconName = 'home';
                        break;
                    case 'Contato':
                        iconName = 'message-square';
                        break;
                    default:
                        iconName = 'circle';
                        break;
                }


                return (
                    <TouchableOpacity
                        accessibilityRole="button"
                        accessibilityStates={isFocused ? ['selected'] : []}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={onPress}
                        onLongPress={onLongPress}
                        style={styles.item}
                    >
                        <View style={isFocused ? {

                            padding: 5,
                            backgroundColor: "#ff8c1f",
                            width: 45,
                            height: 45,
                            borderRadius: 40,
                            justifyContent: 'center',
                            alignItems: 'center'




                        } : {
                                padding: 5,
                                width: 45,
                                height: 45,
                                paddingTop: 12,




                            }} >
                            <Icon name={iconName} type='feather' size={24} color={isFocused ? '#fff' : '#000000'} />
                        </View>
                        {/* <Text style={{ color: isFocused ? '#ff8c1f' : '#000000' }}>
                            {label}
                        </Text> */}

                    </TouchableOpacity>
                );
            })}
        </View>
    );
}



const styles = StyleSheet.create({
    item: {
        flex: 1,
        textAlign: "center",
        paddingVertical: 8,
        alignItems: 'center',
        borderRadius: 40,
    },
});
