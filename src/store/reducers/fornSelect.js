const initialState = [];
export default function fornSelect(state = initialState, action) {
  switch (action.type) {
    case "OBJETO":
      return [...state, action.fornSelect];

    case "FILE":
      var newState = state;
      newState.map((item) => {
        if (item != undefined) {
          if (item.id === action.fornSelect.id) {
            item.arquivo = action.fornSelect.arquivo;
          }
        }
      });
      return [...newState];

    case "ADD_ITEM":
      var newState = state;
      var existe = false;
      if (newState.length === 0) {
        newState.push(action.fornSelect);
      } else {
        newState.map((item) => {
          if (item != undefined) {
            if (item.id === action.fornSelect.id) {
              item.empresa = action.fornSelect.empresa;
              existe = true;
            }
          }
        });
        if (!existe) {
          newState.push(action.fornSelect);
        }
      }
      return [...newState];
    // return [...state, action.getItensCotacao]

    case "REMOVE_ITEM":
      var newState = state;
      newState.map((item, key) => {
        if (item != undefined) {
          if (item.id === action.id) {
            newState.splice(key, 1);
          }
        }
      });
      return newState;

    case "CLEAR":
      var newState = [];
      return newState;

    default:
      return state;
  }
}
