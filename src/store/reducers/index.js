import {combineReducers} from 'redux';
import autorizados from './autorizados';
import getItensCotacao from './getItensCotacao';
import fornSelect from './fornSelect';
import autorizacaoCotacao from './autorizacaoCotacao';
import drawer from './drawer';
import navDrop from './navDrop';
import painelDms from './painelDms';


export default combineReducers({
    autorizados,
    getItensCotacao,
    fornSelect,
    autorizacaoCotacao,
    drawer,
    navDrop,
    painelDms
});