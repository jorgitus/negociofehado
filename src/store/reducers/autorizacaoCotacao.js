const initialState = [];
export default function autorizacaoCotacao(state = initialState,action)
{   
    switch(action.type) {
        case 'ADD_ITEM':
            return [...state, action.getItensCotacao]

        case 'REMOVE_ITEM':
            var newState = state;
            newState.splice(newState.indexOf(action.getItensCotacao), 1);
            return newState;

        default:
          return state;
      }
}


