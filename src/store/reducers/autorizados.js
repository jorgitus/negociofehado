const initialState = [];

export default function autorizados(state = initialState,action)
{      

    switch(action.type) {
        case 'ADD_ITEM':
            state.forEach(element => {
                if(element != undefined){
                    if(element.name === action.autorizados.name){
                        element.checked = action.autorizados.checked;
                    }
                }
            });
            return [...state, action.autorizados]; 

            case 'CLEAR':
            return []; 
            
        default:
          return state;
      }
   
}
