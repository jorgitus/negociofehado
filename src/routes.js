import React from 'react';
import {NavigationContainer, DefaultTheme} from "@react-navigation/native"
import {createStackNavigator} from "@react-navigation/stack"
import { createDrawerNavigator } from '@react-navigation/drawer';

import Inicio from "./pages/Home"
import Categoria from "./pages/Categoria"
import MinhaLoja from "./pages/MinhaLoja"
import DetalheItem from "./pages/DetalheItem"
import Login from "./pages/Login"
import Localizacao from "./pages/Localizacao"
import Orcamento from "./pages/Orcamento"
import Publicacao from "./pages/Publicacao"
import Solicitacoes from "./pages/Solicitacoes"
import Chat from "./pages/Chat"
import InserirPublicacao from "./pages/InserirPublicacao"
import Splash from "./pages/Splashsreen"
import Fornecedor from "./pages/Fornecedor"








import {DrawerContent} from "./components/Drawer"
import {MyTabBar} from "./components/TabBar"


const AppStack = createDrawerNavigator();
const Stack = createStackNavigator();



function Home() {
    return (
        <Stack.Navigator   screenOptions={{
            headerShown: false
          }}>
                <Stack.Screen name="Home" component={Inicio}></Stack.Screen>
                <Stack.Screen name="Categoria" component={Categoria}/>
                <Stack.Screen name="DetalheItem" component={DetalheItem}/>
                <Stack.Screen name="Localizacao" component={Localizacao}/>
                <Stack.Screen name="Orcamento" component={Orcamento}/>
                <Stack.Screen name="Publicacao" component={Publicacao}/>
                <Stack.Screen name="Solicitacoes" component={Solicitacoes}/>
                <Stack.Screen name="Fornecedor" component={Fornecedor}/>

        </Stack.Navigator>
    );
  }


  function DrawerScream() {
    return (
        <AppStack.Navigator drawerContent={props => <DrawerContent {...props} />} headerMode="none" screenOptions={{headerShown:false,cardStyle:{backgroundColor:"#ecf1f8"}}}>
      
        <AppStack.Screen name="Login" component={Login}/>
        <AppStack.Screen name="Home" component={Home}></AppStack.Screen>
        <AppStack.Screen name="Localizacao" component={Localizacao}/>

        <AppStack.Screen name="MinhaLoja" component={MinhaLoja}></AppStack.Screen>
        <AppStack.Screen name="Solicitacoes" component={Solicitacoes}/>

        {/* <AppStack.Screen name="Publicacao" component={Publicacao}/> */}
        <AppStack.Screen name="Chat" component={Chat}/>
        <AppStack.Screen name="InserirPublicacao" component={InserirPublicacao}/>
        </AppStack.Navigator>
    );
  }
  
  

export default function Routes() {
    return (
        <NavigationContainer style={{backgroundColor:"#333"}}>
           <Stack.Navigator   screenOptions={{
            headerShown: false
          }}>
                <Stack.Screen name="Splash" component={Splash}></Stack.Screen>
                <Stack.Screen name="DrawerScream" component={DrawerScream}></Stack.Screen>
        </Stack.Navigator>
        </NavigationContainer>
    )
  
}